/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

import Modelo.Dao.ProductoDAO;
import Modelo.Entity.Producto;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ANDERSON
 */
@WebServlet("/producto")
public class ProductoController extends HttpServlet {

    ProductoDAO pr = new ProductoDAO();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "borrar":
                    this.metodos(req, resp, 2);
                    break;
                case "editar":
                    this.metodos(req, resp, 3);
                    break;
                default:
                    this.listarProducto(req, resp);

            }
        } else {
            this.listarProducto(req, resp);
        }

    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "crear":
                    this.metodos(req, resp, 1);
                    break;
                case "modificar":
                    this.metodos(req, resp, 4);
                    break;
                default:
                    this.listarProducto(req, resp);
            }
        } else {
            this.listarProducto(req, resp);
        }

    }

    private void listarProducto(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Producto> productos = pr.consultar();
        req.setAttribute("productos", productos);
        req.getRequestDispatcher("producto/verProductos.jsp").forward(req, resp);
    }

    public void metodos(HttpServletRequest req, HttpServletResponse resp, int n) throws ServletException, IOException {
        int id;
        int precio;
        String nombre;

        switch (n) {
            case 1: {  //Crear
                id = Integer.valueOf(req.getParameter("id_producto"));
                precio = Integer.valueOf(req.getParameter("precio"));
                nombre = req.getParameter("nombre");
                int registros = new ProductoDAO().insertar(new Producto(id, precio, nombre));
                this.listarProducto(req, resp);
                break;
            }

            case 2: {  //Borrar
                id = Integer.valueOf(req.getParameter("id_producto"));
                int registros = this.pr.borrar(id);
                this.listarProducto(req, resp);

                break;
            }

            case 3: { //Editar
                id = Integer.valueOf(req.getParameter("id_producto"));
                Producto pro = this.consultarP(id);
                //Producto pro = new Producto(123456, 1000, "producto01");
                req.setAttribute("producto", pro);
                req.getRequestDispatcher("producto/editarProducto.jsp").forward(req, resp);
                this.listarProducto(req, resp);
                break;
            }
            case 4: { //Modificar
                id = Integer.valueOf(req.getParameter("id_producto"));
                precio = Integer.valueOf(req.getParameter("precio"));
                nombre = req.getParameter("nombre");
                int registros = new ProductoDAO().actualizar(new Producto(id, precio, nombre));
                this.listarProducto(req, resp);
                break;
            }
        }

    }

    private Producto consultarP(int id) {
        Producto pro = null;

        for (Producto p : pr.consultar()) {
            if (p.getId_producto() == id) {
                pro = new Producto(p.getId_producto(), p.getPrecio(), p.getNombre());
            }
        }
        return pro;
    }

}
