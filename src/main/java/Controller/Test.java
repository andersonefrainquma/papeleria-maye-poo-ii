/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Modelo.Dao.AlmacenDAO;
import Modelo.Dao.CajaRegistradoraDAO;
import Modelo.Dao.ClienteDAO;
import Modelo.Dao.FacturaDAO;
import Modelo.Dao.ProductoDAO;
import Modelo.Dao.ProductosPDAO;
import Modelo.Dao.ProveedorDAO;
import Modelo.Entity.Almacen;
import Modelo.Entity.CajaRegistradora;
import Modelo.Entity.Cliente;
import Modelo.Entity.Factura;
import Modelo.Entity.Producto;
import Modelo.Entity.ProductosP;
import Modelo.Entity.Proveedor;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author ANDERSON
 */
public class Test {

    //Tablas a elegir
    private static final String tablas[] = {"Producto", "Cliente", "Almacen", "Factura",
        "CajaRegistradora", "Proveedor", "ProductosP"};

    //DAO
    private static final ProductoDAO producto = new ProductoDAO();
    private static final ClienteDAO cliente = new ClienteDAO();
    private static final AlmacenDAO almacen = new AlmacenDAO();
    private static final FacturaDAO factura = new FacturaDAO();
    private static final CajaRegistradoraDAO caja = new CajaRegistradoraDAO();
    private static final ProveedorDAO proveedor = new ProveedorDAO();
    private static final ProductosPDAO productosP = new ProductosPDAO();

    //Entrada de datos
    private static final Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        boolean comprobar = true;
        System.out.println("--------------Base de Datos PapeleriaMaye--------------");
        while (comprobar) {
            //\n
            System.out.println("\n¿Cuál tabla desea seleccionar?\n");
            String msg = "";
            for (int i = 0; i < tablas.length; i++) {
                msg += (i + 1) + ". " + tablas[i] + "\n";
            }
            System.out.println(msg);
            tablas(in.nextByte());
            System.out.println("\n¿Desea hacer otra ejecución? (1-Sí),(2-No)");
            comprobar = in.nextByte() == 1;
        }
    }

    private static void tablas(byte n) {
        imprimir(n);
        byte a = in.nextByte();
        switch (n) {
            case 1: { //Producto
                int id;
                int precio;
                String nombre;
                switch (a) {
                    case 1: //Agregar
                        System.out.println("ID: ");
                        id = in.nextInt();
                        System.out.println("Precio: ");
                        precio = in.nextInt();
                        System.out.println("Nombre: ");
                        nombre = in.nextLine();
                        nombre = in.nextLine();

                        producto.insertar(new Producto(id, precio, nombre));
                        break;

                    case 2: //Consultar
                        for (Producto pro : producto.consultar()) {
                            System.out.println(pro);
                        }
                        break;

                    case 3: //Eliminar
                        System.out.println("ID del producto a eliminar: ");
                        id = in.nextInt();

                        producto.borrar(id);
                        break;

                    case 4: //Actualizar
                        System.out.println("ID del producto a actualizar: ");
                        id = in.nextInt();
                        System.out.println("Precio: ");
                        precio = in.nextInt();
                        System.out.println("Nombre: ");
                        nombre = in.nextLine();
                        nombre = in.nextLine();

                        producto.actualizar(new Producto(id, precio, nombre));
                        break;
                }
                break;
            }

            case 2: { //Cliente
                int cedula;
                String nombre;
                switch (a) {
                    case 1: //Agregar
                        System.out.println("Cédula: ");
                        cedula = in.nextInt();
                        System.out.println("Nombre: ");
                        nombre = in.nextLine();
                        nombre = in.nextLine();

                        cliente.insertar(new Cliente(cedula, nombre));
                        break;

                    case 2: //Consultar
                        for (Cliente c : cliente.consultar()) {
                            System.out.println(c);
                        }

                        break;

                    case 3: //Eliminar
                        System.out.println("Cédula del cliente a eliminar: ");
                        cedula = in.nextInt();
                        cliente.borrar(cedula);
                        break;

                    case 4: //Actualizar
                        System.out.println("Cédula del cliente a actualizar: ");
                        cedula = in.nextInt();
                        System.out.println("Nombre: ");
                        nombre = in.nextLine();
                        nombre = in.nextLine();

                        cliente.actualizar(new Cliente(cedula, nombre));
                        break;
                }

                break;
            }

            case 3: { //Almacen
                int cantidad;
                long precioA;

                switch (a) {
                    case 1: //Consultar  
                        List<Almacen> alm = almacen.consultar();
                        System.out.println(alm);
                        break;

                    case 2: //Actualizar
                        System.out.println("Nueva cantidad: ");
                        cantidad = in.nextInt();
                        System.out.println("Precio almacenado: ");
                        precioA = in.nextInt();

                        almacen.actualizar(new Almacen(cantidad, precioA));
                        break;
                }
                break;
            }

            case 4: { //Factura
                int id;
                long precioTotal;
                switch (a) {
                    case 1: //Agregar
                        System.out.println("ID: ");
                        id = in.nextInt();
                        System.out.println("PrecioTotal: ");
                        precioTotal = in.nextInt();

                        factura.insertar(new Factura(id, precioTotal));
                        break;

                    case 2: //Consultar

                        for (Factura f : factura.consultar()) {
                            System.out.println(f);
                        }

                        break;

                    case 3: //Eliminar
                        System.out.println("ID de la factura a eliminar: ");
                        id = in.nextInt();

                        factura.borrar(id);
                        break;

                    case 4: //Actualizar
                        System.out.println("ID de la factura a actualizar: ");
                        id = in.nextInt();
                        System.out.println("PrecioTotal: ");
                        precioTotal = in.nextInt();

                        factura.actualizar(new Factura(id, precioTotal));
                        break;
                }
                break;
            }

            case 5: { //CajaRegistradora
                long dineroR;
                long vueltos;
                int contadorV;
                long dineroAcumulado;

                switch (a) {

                    case 1: //Consultar

                        for (CajaRegistradora al : caja.consultar()) {
                            System.out.println(al);
                        }

                        break;

                    case 2: //Actualizar
                        System.out.println("Dinero recibido: ");
                        dineroR = in.nextLong();
                        System.out.println("Vueltos: ");
                        vueltos = in.nextLong();
                        System.out.println("Contador de ventas: ");
                        contadorV = in.nextInt();
                        System.out.println("Dinero acumulado: ");
                        dineroAcumulado = in.nextLong();

                        caja.actualizar(new CajaRegistradora(dineroR, vueltos, contadorV, dineroAcumulado));
                        break;
                }

                break;
            }

            case 6: { //Proveedor
                int cedula;
                String nombre;

                switch (a) {

                    case 1://Agregar
                        System.out.println("Cédula: ");
                        cedula = in.nextInt();
                        System.out.println("Nombre: ");
                        nombre = in.nextLine();
                        nombre = in.nextLine();

                        proveedor.insertar(new Proveedor(cedula, nombre));
                        break;

                    case 2: //Consultar
                        for (Proveedor p : proveedor.consultar()) {
                            System.out.println(p);
                        }
                        break;

                    case 3: //Eliminar
                        System.out.println("Cédula del proveedor a eliminar: ");
                        cedula = in.nextInt();
                        proveedor.borrar(cedula);
                        break;

                    case 4: //Actualizar
                        System.out.println("Cédula del proveedor a actualizar: ");
                        cedula = in.nextInt();
                        System.out.println("Nombre: ");
                        nombre = in.nextLine();
                        nombre = in.nextLine();

                        proveedor.actualizar(new Proveedor(cedula, nombre));
                        break;
                }

                break;
            }

            case 7: {//Productos del proveedor
                int id;
                int precio;
                String nombre;

                switch (a) {
                    case 1: //Agregar
                        System.out.println("ID: ");
                        id = in.nextInt();
                        System.out.println("Precio: ");
                        precio = in.nextInt();
                        System.out.println("Nombre: ");
                        nombre = in.nextLine();
                        nombre = in.nextLine();

                        productosP.insertar(new ProductosP(id, precio, nombre));
                        break;

                    case 2: //Consultar
                        for (ProductosP pro : productosP.consultar()) {
                            System.out.println(pro);
                        }
                        break;

                    case 3: //Eliminar
                        System.out.println("ID del producto a eliminar: ");
                        id = in.nextInt();

                        productosP.borrar(id);
                        break;

                    case 4: //Actualizar
                        System.out.println("ID del producto a actualizar: ");
                        id = in.nextInt();
                        System.out.println("Precio: ");
                        precio = in.nextInt();
                        System.out.println("Nombre: ");
                        nombre = in.nextLine();
                        nombre = in.nextLine();

                        productosP.actualizar(new ProductosP(id, precio, nombre));
                        break;
                }

                break;
            }
        }
    }

    private static void imprimir(byte t) {
        System.out.println("---" + tablas[t - 1] + "---");
        System.out.println("\n¿Qué desea hacer con la tabla seleccionada?\n");
        if (t == 1 || t == 2 || t == 4 || t == 6 || t == 7) {
            String opciones[] = {"Agregar", "Consultar", "Eliminar", "Actualizar"};
            String msg = "";
            for (int i = 0; i < opciones.length; i++) {
                msg += (i + 1) + ". " + opciones[i] + "\n";
            }
            System.out.println(msg);
        } else {
            String opciones[] = {"Consultar", "Actualizar"};
            String msg = "";
            for (int i = 0; i < opciones.length; i++) {
                msg += (i + 1) + ". " + opciones[i] + "\n";
            }
            System.out.println(msg);
        }
    }
}
