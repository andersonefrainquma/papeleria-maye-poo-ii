/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Entity;

/**
 *
 * @author ANDERSON
 */
public class ProductosP extends Producto{

    public ProductosP() {
    }

    public ProductosP(int id_producto, int precio, String nombre) {
        super(id_producto, precio, nombre);
    }

    
    @Override
    public int getId_producto() {
        return id_producto;
    }

    @Override
    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    @Override
    public int getPrecio() {
        return precio;
    }

    @Override
    public void setPrecio(int precio) {
        this.precio = precio;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    @Override
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "ProductosP{" + '}';
    }
    


}
