/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Entity;

/**
 *
 * @author ANDERSON
 */
public class CajaRegistradora {
    
    long dineroRecibido;
    long vueltos;
    int contadorVentas;
    long dineroAcumulado;

    public CajaRegistradora() {
    }

    public CajaRegistradora(long dineroRecibido, long vueltos, int contadorVentas, long dineroAcumulado) {
        this.dineroRecibido = dineroRecibido;
        this.vueltos = vueltos;
        this.contadorVentas = contadorVentas;
        this.dineroAcumulado = dineroAcumulado;
    }

    public long getDineroRecibido() {
        return dineroRecibido;
    }

    public void setDineroRecibido(long dineroRecibido) {
        this.dineroRecibido = dineroRecibido;
    }

    public long getVueltos() {
        return vueltos;
    }

    public void setVueltos(long vueltos) {
        this.vueltos = vueltos;
    }

    public int getContadorVentas() {
        return contadorVentas;
    }

    public void setContadorVentas(int contadorVentas) {
        this.contadorVentas = contadorVentas;
    }

    public long getDineroAcumulado() {
        return dineroAcumulado;
    }

    public void setDineroAcumulado(long dineroAcumulado) {
        this.dineroAcumulado = dineroAcumulado;
    }

    @Override
    public String toString() {
        return "CajaRegistradora{" + "dineroRecibido=" + dineroRecibido + ", vueltos=" + vueltos + ", contadorVentas=" + contadorVentas + ", dineroAcumulado=" + dineroAcumulado + '}';
    }


    
    
    
}
