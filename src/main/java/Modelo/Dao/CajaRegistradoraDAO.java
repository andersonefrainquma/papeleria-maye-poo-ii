/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.CajaRegistradora;
import Modelo.Red.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JUAN CAMILO
 */
public class CajaRegistradoraDAO implements CajaRegistradoraService {

    public static final String SQL_CONSULTA = "SELECT * FROM cajaregistradora";
    public static final String SQL_UPDATE = "UPDATE cajaregistradora SET dineroRecibido = ?, vueltos = ?, contadorVentas = ?, dineroAcumulado = ? WHERE id = 123456";

    @Override
    public List<CajaRegistradora> consultar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<CajaRegistradora> cajaRegistradora = new ArrayList();
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                long dineroRecibido = res.getLong("dineroRecibido");
                long vueltos = res.getLong("vueltos");
                int contadorV = res.getInt("contadorVentas");
                long dineroA = res.getLong("dineroAcumulado");

                CajaRegistradora caja = new CajaRegistradora(dineroRecibido, vueltos, contadorV, dineroA);
                cajaRegistradora.add(caja);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return cajaRegistradora;
    }

    @Override
    public int actualizar(CajaRegistradora cajaRegistradora) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setLong(1, cajaRegistradora.getDineroRecibido());
            ps.setLong(2, cajaRegistradora.getVueltos());
            ps.setInt(3, cajaRegistradora.getContadorVentas());
            ps.setLong(4, cajaRegistradora.getDineroAcumulado());

            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }

        return registros;
    }

}
