/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.Proveedor;
import java.util.List;

/**
 *
 * @author JUAN CAMILO
 */
public interface ProveedorService {
    public int insertar(Proveedor proveedor);

    public List<Proveedor> consultar();

    public int borrar(int cedula);

    public int actualizar(Proveedor proveedor);
}
