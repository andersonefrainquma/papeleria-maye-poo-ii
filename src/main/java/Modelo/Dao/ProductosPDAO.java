/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.ProductosP;
import Modelo.Red.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JUAN CAMILO
 */
public class ProductosPDAO implements ProductosPService {

    public static final String SQL_CONSULTA = "SELECT * FROM productop";
    public static final String SQL_INSERTAR = "INSERT INTO productop(id_producto, precio, nombre) VALUES (?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM productop WHERE id_producto = ?";
    public static final String SQL_UPDATE = "UPDATE productop SET precio = ?, nombre = ? WHERE id_producto = ?";

  
    @Override
    public int insertar(ProductosP producto) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_INSERTAR);
            ps.setInt(1, producto.getId_producto());
            ps.setInt(2, producto.getPrecio());
            ps.setString(3, producto.getNombre());
            registros = ps.executeUpdate();
            
            /*
                  con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_INSERTAR);
            ps.setInt(1, producto.getId_producto());
            ps.setInt(2, producto.getPrecio());
            ps.setString(3, producto.getNombre());
            registros = ps.executeUpdate();
            */
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<ProductosP> consultar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<ProductosP> productos = new ArrayList();
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id_producto");
                int precio = res.getInt("precio");
                String nombre = res.getString("nombre");
                ProductosP producto = new ProductosP(id, precio, nombre);
                productos.add(producto);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return productos;

    }

    @Override
    public int borrar(int id_producto) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, id_producto);
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }

        return registros;
    }

    @Override
    public int actualizar(ProductosP producto) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);

            ps.setInt(1, producto.getPrecio());
            ps.setString(2, producto.getNombre());
            ps.setInt(3, producto.getId_producto());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
