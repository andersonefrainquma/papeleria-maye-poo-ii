/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.Almacen;
import java.util.List;

/**
 *
 * @author JUAN CAMILO
 */
public interface AlmacenService {
    public List<Almacen> consultar();

    public int actualizar(Almacen almacen);
}
