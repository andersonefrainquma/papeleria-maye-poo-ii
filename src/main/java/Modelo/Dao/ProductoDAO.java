/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.Producto;
import Modelo.Red.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ANDERSON
 */
public class ProductoDAO implements ProductoService {

    public static final String SQL_CONSULTA = "SELECT * FROM producto";
    public static final String SQL_INSERTAR = "INSERT INTO producto(id, precio, nombre) VALUES (?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM producto WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE producto SET precio = ?, nombre = ? WHERE id = ?";

    @Override
    public int insertar(Producto producto) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_INSERTAR);
            ps.setInt(1, producto.getId_producto());
            ps.setInt(2, producto.getPrecio());
            ps.setString(3, producto.getNombre());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Producto> consultar() {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Producto> productos = new ArrayList();
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id");
                int precio = res.getInt("precio");
                String nombre = res.getString("nombre");
                Producto producto = new Producto(id, precio, nombre);
                productos.add(producto);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return productos;
    }

    @Override
    public int borrar(int id_producto) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, id_producto);
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }

        return registros;
    }

    @Override
    public int actualizar(Producto producto) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);

            ps.setInt(1, producto.getPrecio());
            ps.setString(2, producto.getNombre());
            ps.setInt(3, producto.getId_producto());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }

        return registros;
    }

}
