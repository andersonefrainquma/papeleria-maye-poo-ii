/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.ProductosP;
import java.util.List;

/**
 *
 * @author JUAN CAMILO
 */
public interface ProductosPService {

    public int insertar(ProductosP producto);

    public List<ProductosP> consultar();

    public int borrar(int id_producto);

    public int actualizar(ProductosP producto);
}
