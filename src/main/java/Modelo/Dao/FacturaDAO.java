/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.Factura;
import Modelo.Red.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JUAN CAMILO
 */
public class FacturaDAO implements FacturaService {

    public static final String SQL_CONSULTA = "SELECT * FROM factura";
    public static final String SQL_INSERTAR = "INSERT INTO factura(id_factura, precioTotal) VALUES (?,?)";
    public static final String SQL_DELETE = "DELETE FROM factura WHERE id_factura = ?";
    public static final String SQL_UPDATE = "UPDATE factura SET precioTotal = ? WHERE id_factura = ?";

    @Override
    public int insertar(Factura factura) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_INSERTAR);
            ps.setInt(1, factura.getId_factura());
            ps.setLong(2, factura.getPrecioTotal());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Factura> consultar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Factura> facturas = new ArrayList();
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int id_factura = res.getInt("id_factura");
                long precio = res.getLong("precioTotal");
                facturas.add(new Factura(id_factura, precio));
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return facturas;
    }

    @Override
    public int borrar(int id_factura) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, id_factura);
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }

        return registros;
    }

    @Override
    public int actualizar(Factura factura) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setLong(1, factura.getPrecioTotal());
            ps.setInt(2, factura.getId_factura());

            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }

        return registros;
    }

}
