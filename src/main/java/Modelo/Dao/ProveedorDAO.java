/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Dao;

import Modelo.Entity.Proveedor;
import Modelo.Red.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ANDERSON
 */
public class ProveedorDAO implements ProveedorService {

    public static final String SQL_CONSULTA = "SELECT * FROM proveedor";
    public static final String SQL_INSERTAR = "INSERT INTO proveedor(cedula, nombre) VALUES (?,?)";
    public static final String SQL_DELETE = "DELETE FROM proveedor WHERE cedula = ?";
    public static final String SQL_UPDATE = "UPDATE proveedor SET nombre = ? WHERE cedula = ?";

    @Override
    public int insertar(Proveedor proveedor) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_INSERTAR);
            ps.setInt(1, proveedor.getCedula());
            ps.setString(2, proveedor.getNombre());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Proveedor> consultar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Proveedor> proveedores = new ArrayList();
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int cedula = res.getInt("cedula");
                String nombre = res.getString("nombre");
                Proveedor proveedor = new Proveedor(cedula, nombre);
                proveedores.add(proveedor);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);

        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return proveedores;
    }

    @Override
    public int borrar(int cedula) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, cedula);
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }

        return registros;
    }

    @Override
    public int actualizar(Proveedor proveedor) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setString(1, proveedor.getNombre());
            ps.setInt(2, proveedor.getCedula());

            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }

        return registros;
    }

}
