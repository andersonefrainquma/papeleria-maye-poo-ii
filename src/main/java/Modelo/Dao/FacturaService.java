/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Dao;

import Modelo.Entity.Factura;
import java.util.List;

/**
 *
 * @author JUAN CAMILO
 */
public interface FacturaService {

    public int insertar(Factura factura);

    public List<Factura> consultar();

    public int borrar(int id_factura);

    public int actualizar(Factura factura);
}
